/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.projekt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.sql.*;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ord.im.OrdImage;
import pdb.gui.gui;

/**
 * Třída pro připojení, inicializaci a práci s databází
 *
 * @author Radim Jílek, xjilek14
 */
public class Databaze {

	public static Connection connection;
	public static Vector<String> files = new Vector<>();
	public static String login;
	public static String password;
        
        public static long month = 2629746000l;

	public Databaze(String login, String password) {
		Databaze.login = login;
		Databaze.password = password;
		Databaze.connection = Pripoj();
	}

	/**
	 * Provede připojení k databázi
	 *
	 * @return nové připojení
	 */
	public static Connection Pripoj() {
		Connection conn = null;

		try {
			OracleDataSource ods = new OracleDataSource();
			ods.setURL("jdbc:oracle:thin:@gort.fit.vutbr.cz:1521:dbgort");
			ods.setUser(login);
			ods.setPassword(password);
			
			conn = ods.getConnection();
		} catch (SQLException ex) {
			//Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
		Databaze.connection = conn;
		// connect to the database

		return conn;
	}

	/**
	 * Načte a provede příkazy SQL skriptu
	 *
	 * @param aSQLScriptFilePath
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public static boolean provedSkript(String aSQLScriptFilePath) throws IOException, SQLException {
		boolean hotovo = false;
		int c = 0;

		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		Statement stmt = conn.createStatement();
		try {
			StringBuilder sb;
			StringBuilder append;
			StringBuilder drop = new StringBuilder();
			try (BufferedReader in = new BufferedReader(new FileReader(aSQLScriptFilePath))) {
				String str;
				sb = new StringBuilder();
				while ((str = in.readLine()) != null) {
					if (c == 0) {
						append = drop.append(str).append("\n ");
					} else {
						append = sb.append(str).append("\n ");
					}
					c++;
				}
			}
			String[] droptable = drop.toString().split(";");
			try {
				stmt.executeUpdate(droptable[0]);
			} catch (Exception e) {
			}

			String[] inst = sb.toString().split(";");

			for (String inst1 : inst) {
				if (!inst1.trim().equals("")) {
					stmt.executeUpdate(inst1);
				}
			}
			hotovo = true;
		} catch (IOException | SQLException e) {
			System.err.println("Nepovedlo se provest skript!");
		}
		conn.close();
		return hotovo;
	}

        /**
         * Vloží záznam do seznamu dodavatelů
         * 
         * @param nazev      Název dodavatele.
         * @param smlouva_od Od kdy je smluvně zavázán dodávat.
         * @param smlouva_do Do kdy je smluvně zavázán dodávat.
         * @throws SQLException
         */        
        public static void vlozDodavatele(String nazev, Date smlouva_od, Date smlouva_do) throws SQLException {
            String prikaz = "insert into suplier(id,nazev,smlouva_od,smlouva_do)"
                            + " values (null,?,?,?)";
            
            Connection conn = connection;
            if (connection.isClosed()) {
                conn = Databaze.Pripoj();
            }
            
            conn.setAutoCommit(false);
            
            try (OraclePreparedStatement pstmtInsert = (OraclePreparedStatement) conn.prepareStatement(prikaz, Statement.RETURN_GENERATED_KEYS)) {
                        pstmtInsert.setString(1, nazev);
			pstmtInsert.setDate(2, smlouva_od);
			pstmtInsert.setDate(3, smlouva_do);

			pstmtInsert.executeUpdate();
			try (ResultSet rs = pstmtInsert.getGeneratedKeys()) {
				//System.out.println(rs.toString());
				if (rs.next()) {
					RowId id1 = rs.getRowId(1);
				}
			}
			pstmtInsert.close();
	    } catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
	    }
            conn.commit();
        }
        
	/**
	 * Vloží záznam do seznamu produktů
	 *
	 * @param nazev
	 * @param geometrie
	 * @param typ
	 * @param dostupnost
	 * @param popis
	 * @param foto cesta k souboru s obrázkem produktu
	 * @throws SQLException
	 * @throws Exception
	 */
	public static void vlozZaznam(String nazev, int geometrie, String typ, Date[] dostupnost, String popis, String foto, Integer dodavatel) throws SQLException, Exception {
		OrdImage imgProxy = null;
		String prikaz = "insert into article(id,nazev,geometrie,typ,dostupne_od,dostupne_do,popis,nahled,foto,dodavatel)"
				+ " values (null,?,?,?,?,?,?,ordsys.ordimage.init(),ordsys.ordimage.init(),?)";

		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		conn.setAutoCommit(false);

		try (OraclePreparedStatement pstmtInsert = (OraclePreparedStatement) conn.prepareStatement(prikaz, Statement.RETURN_GENERATED_KEYS)) {
			pstmtInsert.setString(1, nazev);
			pstmtInsert.setInt(2, geometrie);
			pstmtInsert.setString(3, typ);
			pstmtInsert.setDate(4, dostupnost[0]);
			pstmtInsert.setDate(5, dostupnost[1]);
			pstmtInsert.setString(6, popis);
			pstmtInsert.setInt(7, dodavatel);

			pstmtInsert.executeUpdate();
			try (ResultSet rs = pstmtInsert.getGeneratedKeys()) {
				//System.out.println(rs.toString());
				if (rs.next()) {
					RowId id1 = rs.getRowId(1);
				}
			}
			pstmtInsert.close();
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		files.add(foto);
		conn.commit();
		//conn.close();
	}

	/**
	 * Vloží do databáze obrázek produktu
	 *
	 * @param id ID požadovaného produktu
	 * @param cesta cesta k souboru s obrázkem produktu
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void vlozObrazek(Integer id, String cesta) throws SQLException, IOException {
		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		connection.setAutoCommit(false);
		OrdImage imgProxy = null;
		OrdImage thumb = null;
		// ziskame proxy z databáze
		String prikaz;
		prikaz = "select id, foto from article where id = ? for update";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			if (id != 0) {
				pstmtSelect.setInt(1, id);
			} else {
				pstmtSelect.setString(1, "max(id)");
			}
			try (OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
				if (rset.next()) {
					imgProxy = (OrdImage) rset.getORAData("foto", OrdImage.getORADataFactory());
					if (id == 0) {
						id = rset.getInt("id");
					}
				}
			}
		}

		// pouzijeme proxy
		imgProxy.loadDataFromFile(cesta); // nahrajeme obrázek ze souboru
		imgProxy.setProperties();
		// ulozime zmeneny obrazek
		prikaz = "update " + "article" + " set foto = ? where id = ?";
		try (OraclePreparedStatement pstmtUpdate1 = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			pstmtUpdate1.setORAData(1, imgProxy);
			pstmtUpdate1.setInt(2, id);
			int executeUpdate = pstmtUpdate1.executeUpdate();
			//System.out.println("VlozObrazek: update result ="+executeUpdate);
		}
		connection.commit();
	}

	/**
	 * Vytvoří náhled daného produktu a uloží jej do databáze
	 * @param id ID požadovaného produktu
	 * @throws SQLException
	 */
	public static void vytvorNahled(int id) {
		String selectSQL = "select nahled,foto from article where id = ? for update";
		OrdImage thumb;
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}

			conn.setAutoCommit(false);
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}

		try (OraclePreparedStatement pstmtSelect2 = (OraclePreparedStatement) conn.prepareStatement(selectSQL)) {
			pstmtSelect2.setInt(1, id);
			try (OracleResultSet rset = (OracleResultSet) pstmtSelect2.executeQuery()) {
				rset.next();
				thumb = (OrdImage) rset.getORAData("nahled", OrdImage.getORADataFactory());
				//System.out.println("Content lengtht (" + id + "): " + thumb.getContentLength());
				System.out.print(".");
				OrdImage img = (OrdImage) rset.getORAData("foto", OrdImage.getORADataFactory()); //tady bude volani pro ziskani miniatury/nahledu
				try {
					if (img.getWidth() >= img.getHeight()) {
						img.processCopy("maxscale=100, 200 fileformat=png", thumb);
					} else {
						img.processCopy("maxscale=200, 100 fileformat=png", thumb);
					}
				} catch (Exception e) {
				}
				String updateSQL = "update article set nahled=? where id=?";
				OraclePreparedStatement upstmt = (OraclePreparedStatement) conn.prepareStatement(updateSQL);
				upstmt.setORAData(1, thumb);
				upstmt.setInt(2, id);
				upstmt.executeUpdate();
				upstmt.close();
			}
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.commit();
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Zajistí výpočet hodnot potřebných pro porovnávání obrazových dat
	 * @param id
	 * @throws SQLException
	 */
	public static void spocitejHodnoty(Integer id) throws SQLException {
		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}
		String prikaz = "update " + "article" + " x set x.foto_si = SI_StillImage(x.foto.getContent()) where id = ?";
		try (PreparedStatement pstmtUpdate2 = conn.prepareStatement(prikaz)) {
			pstmtUpdate2.setInt(1, id);
			pstmtUpdate2.executeUpdate();
		}

		prikaz = "update " + "article" + " set ";
		try (PreparedStatement pstmtUpdate3 = conn.prepareStatement(
				prikaz
				+ "foto_ac = SI_AverageColor(foto_si), "
				+ "foto_ch = SI_ColorHistogram(foto_si), "
				+ "foto_pc = SI_PositionalColor(foto_si), "
				+ "foto_tx = SI_Texture(foto_si) "
				+ "where id = ?")) {
			pstmtUpdate3.setInt(1, id);
			pstmtUpdate3.executeUpdate();
		}
		//conn.close();
	}

	/**
	 * Získá obrazová data z databáze
	 *
	 * @param id ID požadovaného produktu
	 * @param typ sloupec tabulky s obrázkem
	 * @return obrázek produktu
	 * @throws SQLException
	 * @throws IOException
	 */
	public static OrdImage getFoto(Integer id, String typ) {
		OrdImage imgProxy = null;
		// ziskame proxy

		String prikaz = "SELECT " + typ + " FROM article WHERE id = ?";
		Connection conn = connection;
		try {
			//System.out.println(""+connection+" ("+connection.isClosed()+")");
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			pstmtSelect.setInt(1, id);

			try (OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
				if (rset.next()) {
					imgProxy = (OrdImage) rset.getORAData(typ, OrdImage.getORADataFactory());
				}
			}
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		//conn.close();
		return imgProxy;
	}

	/**
	 * Nahraje do databáze ukázková data
	 *
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void initDb() {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}

                long dost_od = System.currentTimeMillis() - 1*month; //kecy o malem rozsahu intu (hlavne, ze jde o long)
                long dost_do = System.currentTimeMillis() + 1*month;
                
		Date[] dostupnost = new Date[]{new Date(dost_od), new Date(dost_do)};
                Date smlouva_od = new Date(dost_od - 12*month);
                Date smlouva_do = new Date(dost_do + 12*month);
		String path = "../src/main/resources/pictures/";
		try {
			vlozZaznam("Židle 1", 0, "Židle", dostupnost, "Dlouhy popis 1", "zidle1.jpg", 1);
			vlozZaznam("Židle 2", 0, "Židle", dostupnost, "Dlouhy popis 2", "zidle2.jpg", 2);
			vlozZaznam("Židle 3", 0, "Židle", dostupnost, "Dlouhy popis 3", "zidle3.jpg", 3);
			vlozZaznam("Stůl 1", 1, "Stůl", dostupnost, "Dlouhy popis 1", "stul1.jpg", 1);
			vlozZaznam("Stůl 2", 1, "Stůl", dostupnost, "Dlouhy popis 2", "stul2.jpg", 2);
			vlozZaznam("Stůl 3", 1, "Stůl", dostupnost, "Dlouhy popis 3", "stul3.jpg", 3);
			vlozZaznam("Kulatý stůl", 2, "Stůl s dírou", dostupnost, "Dlouhy popis 3", "kulaty_stul.jpg", 3);
			vlozZaznam("Gauč 1", 3, "Gauč", dostupnost, "Dlouhy popis 3", "gauc1.jpg", 3);
			vlozZaznam("Gauč 2", 3, "Gauč", dostupnost, "Dlouhy popis 3", "gauc2.jpg", 4);
			vlozZaznam("Gauč 3", 3, "Gauč", dostupnost, "Dlouhy popis 3", "gauc3.jpg", 1);
			vlozZaznam("Gauč 4", 3, "Gauč", dostupnost, "Dlouhy popis 3", "gauc4.jpg", 2);
			vlozZaznam("Křeslo 1", 4, "Křeslo", dostupnost, "Dlouhy popis 3", "kreslo1.jpg", 3);
			vlozZaznam("Křeslo 2", 4, "Křeslo", dostupnost, "Dlouhy popis 3", "kreslo2.jpg", 2);
			vlozZaznam("Křeslo 3", 4, "Křeslo", dostupnost, "Dlouhy popis 3", "kreslo3.jpg", 4);
			vlozZaznam("Křeslo 4", 4, "Křeslo", dostupnost, "Dlouhy popis 3", "kreslo4.jpg", 2);

			Databaze.prepareAll(path);
                        
                        vlozDodavatele("Dodavatel 1", smlouva_od,smlouva_do);
                       vlozDodavatele("Dodavatel 2", smlouva_od,smlouva_do);
                        vlozDodavatele("Dodavatel 3", smlouva_od,smlouva_do);
                        vlozDodavatele("Dodavatel 4", smlouva_od,smlouva_do);
		} catch (Exception ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Smaže z databáze obrázek a náhled produktu
	 * @param id
	 * @throws SQLException
	 */
	public static void smazObrazek(Integer id) throws SQLException {
		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}
		connection.setAutoCommit(false);
		String prikaz = "select foto,nahled  from article where id = ? for update";

		prikaz = "update " + "article" + " set foto = ordsys.ordimage.init(), nahled = ordsys.ordimage.init() where id = ?";
		try (OraclePreparedStatement pstmtUpdate1 = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			pstmtUpdate1.setInt(1, id);
			pstmtUpdate1.executeUpdate();
		}
		//}
		//}
		connection.commit();
		conn.close();
	}

	/**
	 * Zajistí vložení obrazových dat vytvoření náhledů a výpočet údajů pro porovnávání při inicializaci
	 * @param path cesta k uloženým multimediálním souborům
	 */
	public static void prepareAll(String path) {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}

		String prikaz = "SELECT id FROM article ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz);
				OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			int i = 0;
			while (rset.next()) {
				Integer id = rset.getInt("id");
				String file = files.get(i);
				vlozObrazek(id, path + file);
				vytvorNahled(id);
				spocitejHodnoty(id);
				i++;
			}
		} catch (SQLException | IOException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Najde podobné produkty na základě obrazové podobnosti
	 * @param id ID aktuálního produktu
	 * @return
	 * @throws SQLException
	 */
	public static Vector najdiPodobne(int id) throws SQLException {

		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		String prikaz = "SELECT z.id as zdroj, c.id as cil, c.nahled as nahled, SI_ScoreByFtrList(new SI_FeatureList(z.foto_ac, 0.3, z.foto_ch, 0.3, z.foto_pc, 0.1, z.foto_tx, 0.3), c.foto_si) as podobnost"
				+ " FROM article z, article c"
				+ " WHERE z.id <> c.id AND z.id = " + id + "" + "AND ROWNUM <= 5"
				+ " ORDER BY podobnost ASC";

		Vector podobne = new Vector();
		OrdImage img;

		OraclePreparedStatement pstmt = (OraclePreparedStatement) conn.prepareStatement(prikaz);
		try {
			OracleResultSet rset = (OracleResultSet) pstmt.executeQuery();
			try {
				while (rset.next()) {
					podobne.add(rset.getInt("cil"));
					img = (OrdImage) rset.getORAData("nahled", OrdImage.getORADataFactory());
				}
			} finally {
				rset.close();
			}
		} finally {
			pstmt.close();
		}
		conn.close();
		return podobne;
	}

	/**
	 * Získá seznam všech produktů z databáze a zajistí vložení do GUI
	 *
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void getArticles() throws SQLException, IOException {
		Connection conn = Databaze.connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}
		String prikaz = "SELECT * FROM article ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			while (rset.next()) {
				createLocalObject(rset);
			}
		}
		conn.close();
	}

	/**
	 * Získá z databáze seznam produktů podle seznamu ID
	 * @param ids seznam obsahující požadovaná ID
	 */
	public static void getArticles(Vector<Integer> ids) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT * FROM article WHERE id IN (" + ids.toString().replace("[", "").replace("]", "") + ") ORDER BY 1";
		System.out.println(prikaz + " <- " + ids.toString());
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException | IOException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Získá z databáze seznam prooduktů požadovaného typu
	 * @param types seznam požadovaných typů
	 */
	public static void getArticles(LinkedList types) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT * FROM article WHERE typ IN (" + types.toString().replace("[", "'").replace("]", "'")
				.replace(", ", "','") + ") ORDER BY 1";
		//System.out.println(prikaz+" <- "+types);
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	/**
	 * Získá z databáze seznam produktů omezený podle ID a typu
	 * @param ids seznam požadovaných ID
	 * @param types seznam požadaovaných typů
	 */
	public static void getArticles(Vector<Integer> ids, LinkedList types) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT * FROM article WHERE id IN (" + ids.toString().replace("[", "").replace("]", "") + ")" +
				" AND typ IN (" + types.toString().replace("[", "'").replace("]", "'").replace(", ", "','") + ") ORDER BY 1";
		System.out.println(prikaz + " <- " + ids.toString()+", "+types);
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException | IOException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	/**
	 * Získá z databáze seznam typů
	 * @return seznam typů
	 */
	public static LinkedList getTypes() {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		LinkedList list = new LinkedList();
		String prikaz = "SELECT DISTINCT typ FROM article ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			while (rset.next()) {
				list.add(rset.getString("typ"));
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		return list;
	}

	/**
	 * Získá z databáze seznam dodavatelů
	 * @return seznam dodavatelů
	 */
	public static LinkedList getSupliers() {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		LinkedList list = new LinkedList();
		String prikaz = "SELECT nazev FROM suplier ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			while (rset.next()) {
				list.add(rset.getString("nazev"));
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		return list;
	}

	/**
	 *	Získá z databáze jméno dodavatele
	 * @param i ID dodavatele
	 * @return
	 */
	private static String getSuplierName(Integer i) {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT nazev FROM suplier " + "WHERE id=" + i + " ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			rset.next();
			return rset.getString("nazev");
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	/**
	 * Získá ID posledního záznamu (nejnovější)
	 * @return ID posledního záznamu
	 */
	public static Integer getNewestId() {
		Connection conn = connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT max(id) as maximum from article";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			rset.next();
			return rset.getInt("maximum");
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		return 0;
	}

	/**
	 * Vytvoří lokální objekt na základě informací z databáze a uloží
	 * @param rset
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void createLocalObject(OracleResultSet rset) throws SQLException, IOException {
		OrdImage thumb = null;
		String id = rset.getString("id");
		String nazev = rset.getString("nazev");
		String popis = rset.getString("popis");
		String typ = rset.getString("typ");
		String dodavatel = Databaze.getSuplierName(rset.getInt("dodavatel"));
		Integer geometrie = rset.getInt("geometrie");
		Date d_od = rset.getDATE("dostupne_od").dateValue();
		Date d_do = rset.getDATE("dostupne_do").dateValue();
		thumb = (OrdImage) rset.getORAData("nahled", OrdImage.getORADataFactory());
		localObjekt obj = new localObjekt(Integer.parseInt(id), typ, nazev, popis, geometrie, d_od, d_do, dodavatel, thumb);
		gui.items.put(Integer.parseInt(id), obj);
		gui.addItem(thumb, nazev, popis, Integer.parseInt(id));
	}

}
