/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.projekt;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;
import oracle.sql.DATE;
import static pdb.projekt.Databaze.connection;
import static pdb.projekt.Databaze.createLocalObject;

/**
 *
 * @author xzubat02
 */
public class Temporal {

	public static long month = 2629746000l;

	/**
	 * Získá z databáze seznam prooduktů požadovaného typu
	 *
	 * @param types seznam požadovaných typů
	 * @param vyber_od String formatovany jako pro java.sql.Date.valueOf(str)
	 * @param vyber_do String formatovany jako pro java.sql.Date.valueOf(str)
	 */
	public static void getArticles(LinkedList types, String vyber_od, String vyber_do) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT * FROM article WHERE typ IN (" + types.toString().replace("[", "'").replace("]", "'")
				.replace(", ", "','") + ") AND "
				+ " ((dostupne_od>=TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + vyber_do + "','YYYY-MM-DD')) OR"
				+ " (dostupne_od<TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + vyber_do + "','YYYY-MM-DD')) OR"
				+ " (dostupne_od<TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + vyber_do + "','YYYY-MM-DD') AND dostupne_do>=TO_DATE('" + vyber_od + "','YYYY-MM-DD')) OR"
				+ " (dostupne_od>=TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + vyber_do + "','YYYY-MM-DD') AND dostupne_od<=TO_DATE('" + vyber_do + "','YYYY-MM-DD')))"
				+ " ORDER BY 1";
		System.out.println(prikaz + " <- " + types);
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Získá z databáze seznam prooduktů
	 *
	 * @param vyber_od String formatovany jako pro java.sql.Date.valueOf(str)
	 * @param vyber_do String formatovany jako pro java.sql.Date.valueOf(str)
	 */
	public static void getArticles(String vyber_od, String vyber_do) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT * FROM article WHERE "
				+ "((dostupne_od>=TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + vyber_do + "','YYYY-MM-DD')) OR"
				+ "(dostupne_od<TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + vyber_do + "','YYYY-MM-DD')) OR"
				+ "(dostupne_od<TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + vyber_do + "','YYYY-MM-DD') AND dostupne_do>=TO_DATE('" + vyber_od + "','YYYY-MM-DD')) OR"
				+ "(dostupne_od>=TO_DATE('" + vyber_od + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + vyber_do + "','YYYY-MM-DD') AND dostupne_od<=TO_DATE('" + vyber_do + "','YYYY-MM-DD')))"
				+ "ORDER BY 1";
		//System.out.println(prikaz+" <- "+types);
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Výběr všech artiklů daného dodavatele, který má smlouvu v období od-do
	 *
	 * @param dodavatel id dodavatele
	 * @param smlouva_od String formatovany jako pro java.sql.Date.valueOf(str)
	 * @param smlouva_do String formatovany jako pro java.sql.Date.valueOf(str)
	 */
	public static void getArticles(int dodavatel, String smlouva_od, String smlouva_do) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		String prikaz = "SELECT a.* FROM article a, suplier s WHERE "
				+ "s.id=" + dodavatel + " AND a.dodavatel=s.id AND "
				+ "((s.smlouva_od>=TO_DATE('" + smlouva_od + "','YYYY-MM-DD') AND s.smlouva_do<=TO_DATE('" + smlouva_do + "','YYYY-MM-DD')) OR"
				+ " (s.smlouva_od<TO_DATE('" + smlouva_od + "','YYYY-MM-DD') AND s.smlouva_do>TO_DATE('" + smlouva_do + "','YYYY-MM-DD')) OR"
				+ " (s.smlouva_od<TO_DATE('" + smlouva_od + "','YYYY-MM-DD') AND s.smlouva_do<=TO_DATE('" + smlouva_do + "','YYYY-MM-DD') AND s.smlouva_do>=TO_DATE('" + smlouva_od + "','YYYY-MM-DD')) OR"
				+ " (s.smlouva_od>=TO_DATE('" + smlouva_od + "','YYYY-MM-DD') AND s.smlouva_do>TO_DATE('" + smlouva_do + "','YYYY-MM-DD') AND s.smlouva_od<=TO_DATE('" + smlouva_do + "','YYYY-MM-DD')))"
				+ "ORDER BY 1";
		System.out.println(prikaz);
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				createLocalObject(rset);
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Výběr všech dodavatelů daného typu artiklu dostupných v období od-do
	 *
	 * @param types seznam požadovaných typů
	 * @param dostupne_od String formatovany jako pro java.sql.Date.valueOf(str)
	 * @param dostupne_do String formatovany jako pro java.sql.Date.valueOf(str)
	 * @return vektor dat dodavatelu
	 */
	public static Vector getSuppliers(LinkedList types, String dostupne_od, String dostupne_do) {
		Connection conn = Databaze.connection;
		try {
			if (connection.isClosed()) {
				conn = Databaze.Pripoj();
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		Vector suppliers = new Vector();

		String prikaz = "SELECT s.* FROM suplier s, article a WHERE a.typ IN (" + types.toString().replace("[", "'").replace("]", "'")
				.replace(", ", "','") + ") AND "
				+ " a.dodavatel=s.id AND "
				+ "((a.dostupne_od>=TO_DATE('" + dostupne_od + "','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('" + dostupne_do + "','YYYY-MM-DD')) OR"
				+ "(a.dostupne_od<TO_DATE('" + dostupne_od + "','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('" + dostupne_do + "','YYYY-MM-DD')) OR"
				+ "(a.dostupne_od<TO_DATE('" + dostupne_od + "','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('" + dostupne_do + "','YYYY-MM-DD') AND a.dostupne_do>=TO_DATE('" + dostupne_od + "','YYYY-MM-DD')) OR"
				+ "(a.dostupne_od>=TO_DATE('" + dostupne_od + "','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('" + dostupne_do + "','YYYY-MM-DD') AND a.dostupne_od<=TO_DATE('" + dostupne_do + "','YYYY-MM-DD')))"
				+ " ORDER BY 1";
		System.out.println(prikaz);
		String nazev;

		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
			OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
			while (rset.next()) {
				nazev = rset.getString("nazev");
				if (suppliers.contains(nazev) == false) {
					suppliers.add(nazev);
				}
			}
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		} //catch (IOException ex) {
		//	Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		//}
		try {
			conn.close();
		} catch (SQLException ex) {
			Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
		}
		return suppliers;
	}

	/**
	 * provede db prikaz
	 *
	 * @param con - spojeni s databazi
	 * @param prikaz - provadeny prikaz
	 */
	protected static void processStatement(Connection con, String prikaz) {
		System.out.println(prikaz.toString());
		try (OraclePreparedStatement pstmtInsert = (OraclePreparedStatement) con.prepareStatement(prikaz, Statement.RETURN_GENERATED_KEYS)) {
			pstmtInsert.executeUpdate();
			try (ResultSet rs = pstmtInsert.getGeneratedKeys()) {
				//System.out.println(rs.toString());
				if (rs.next()) {
					RowId id1 = rs.getRowId(1);
				}
			}
			pstmtInsert.close();
		} catch (SQLException ex) {
			Logger.getLogger(Databaze.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Zmeni dodavatele produktu
	 *
	 * @param dodavatel Id dodavatele.
	 * @param id Id produktu.
	 * @param interval_od Od kdy je produkt dostupny.
	 * @param interval_do Do kdy je produkt dostupny.
	 * @throws SQLException
	 */
	public static void zmenDodavatele(int dodavatel, int id, Date interval_od, Date interval_do) throws SQLException {
		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		Date artikl_od = interval_od;
		Date artikl_do = interval_do;
		String interval_od_str = interval_od.toString();
		String interval_do_str = interval_do.toString();

		String prikaz = "SELECT * FROM article WHERE id=" + id + " ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			while (rset.next()) {
				artikl_od = rset.getDate("dostupne_od");
				artikl_do = rset.getDate("dostupne_do");
			}
		}

		if (artikl_od.after(interval_od) && artikl_do.before(interval_do)) {
			//interval cely v zadanem obdobi
			prikaz = "UPDATE article SET dodavatel=" + dodavatel
					+ " WHERE id=" + id + " AND dodavatel<>" + dodavatel
					+ " AND dostupne_od>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.before(interval_od) && artikl_do.after(interval_do)) {
			//interval presahujici zadane obdobi z obou stran
			prikaz = "INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)"
					+ "SELECT a.nazev, a.geometrie, a.typ, a.dostupne_od, TO_DATE('" + interval_od_str
					+ "','YYYY-MM-DD') - 1/86400,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel"
					+ "FROM article a WHERE a.id=" + id + //" AND a.dodavatel<>"+dodavatel+
					" AND a.dostupne_od < TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND a.dostupne_do > TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);

			prikaz = "INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)"
					+ "SELECT a.nazev, a.geometrie, a.typ, TO_DATE('" + interval_do_str
					+ "','YYYY-MM-DD') + 1/86400, a.dostupne_do,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel"
					+ "FROM article a WHERE a.id=" + id +//" AND a.dodavatel<>"+dodavatel+
					" AND a.dostupne_od < TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND a.dostupne_do > TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);

			prikaz = "UPDATE article SET dodavatel=" + dodavatel + ", "
					+ "dostupne_od=TO_DATE('" + interval_od_str + "','YYYY-MM-DD'), dostupne_do=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')"
					+ " WHERE id=" + id + " AND dodavatel<>" + dodavatel
					+ " AND dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.before(interval_od) && artikl_do.before(interval_do) && artikl_do.after(interval_od)) {
			//interval zleva delsi nez zadane obdobi
			prikaz = "INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)"
					+ " SELECT a.nazev, a.geometrie, a.typ, a.dostupne_od, TO_DATE('" + interval_od_str
					+ "','YYYY-MM-DD') - 1/86400,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel"
					+ " FROM article a WHERE a.id=" + id +//" AND a.dodavatel<>"+dodavatel+
					" AND a.dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')  AND a.dostupne_do>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);

			prikaz = "UPDATE article SET dodavatel=" + dodavatel + ", "
					+ "dostupne_od=TO_DATE('" + interval_od_str + "','YYYY-MM-DD'), dostupne_do=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')"
					+ " WHERE id=" + id + " AND dodavatel<>" + dodavatel
					+ " AND dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')  AND dostupne_do>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.after(interval_od) && artikl_do.after(interval_do) && artikl_od.before(interval_do)) {
			//interval zprava delsi nez zadane obdobi     
			System.out.println(dodavatel);
			//processStatement(conn, prikaz);
			prikaz = "SELECT a.nazev, a.geometrie, a.typ, TO_DATE(?,'YYYY-MM-DD') + 1/86400, a.dostupne_do,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel"
					+ " FROM article a WHERE a.id=? AND a.dodavatel<>? AND a.dostupne_od>=TO_DATE(?,'YYYY-MM-DD') AND a.dostupne_do>TO_DATE(?,'YYYY-MM-DD') AND dostupne_od<=TO_DATE(?,'YYYY-MM-DD') for update";
			try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
				pstmtSelect.setString(1, interval_do_str);
				pstmtSelect.setInt(2, id);
				pstmtSelect.setInt(3, dodavatel);
				pstmtSelect.setString(4, interval_od_str);
				pstmtSelect.setString(5, interval_do_str);
				pstmtSelect.setString(6, interval_do_str);
				OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery();
				//prikaz ="INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)";
				prikaz = "INSERT INTO article(nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel) VALUES (?, ?, ?, ?, ?,?, ?, ?,?, ?,?, ?, ?, ?)";
				try (OraclePreparedStatement pstmtInsert = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
					if (rset.next()) {
						pstmtInsert.setString(1, rset.getString("nazev"));
						pstmtInsert.setInt(2, rset.getInt("geometrie"));
						pstmtInsert.setString(3, rset.getString("typ"));
						//pstmtInsert.setDate(7,(DATE.fromText(interval_do_str, "YYYY-MM-DD", "cz")+1/86400)); //od 4
						String newDate = (new Date(interval_do.getTime() + 1 / 86400)).toString();
						System.out.println(newDate);
						pstmtInsert.setDATE(4, DATE.fromText(newDate, "YYYY-MM-DD", "cz"));
						pstmtInsert.setDATE(5, rset.getDATE("dostupne_do"));
						pstmtInsert.setString(6, rset.getString("popis"));
						pstmtInsert.setORAData(7, (OrdImage) rset.getORAData("nahled", OrdImage.getORADataFactory()));
						pstmtInsert.setORAData(8, (OrdImage) rset.getORAData("foto", OrdImage.getORADataFactory()));
						pstmtInsert.setSTRUCT(9, rset.getSTRUCT("foto_si"));
						pstmtInsert.setSTRUCT(10, rset.getSTRUCT("foto_ac"));
						pstmtInsert.setSTRUCT(11, rset.getSTRUCT("foto_ch"));
						pstmtInsert.setSTRUCT(12, rset.getSTRUCT("foto_pc"));
						pstmtInsert.setSTRUCT(13, rset.getSTRUCT("foto_tx"));
						pstmtInsert.setInt(14, rset.getInt("dodavatel"));
					}
					pstmtInsert.executeQuery();
				}

				/*
				 prikaz = "UPDATE article SET dodavatel=?, "
				 + " dostupne_od=TO_DATE('" + interval_od_str + "','YYYY-MM-DD'), dostupne_do=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')"
				 + " WHERE id=" + id + " AND dodavatel<>" + dodavatel
				 + " AND dostupne_od>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + interval_do_str + "','YYYY-MM-DD')  AND dostupne_od<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')";
				 */
				prikaz = "UPDATE article SET dodavatel=?,dostupne_od=TO_DATE(?,'YYYY-MM-DD'), dostupne_do=TO_DATE(?,'YYYY-MM-DD')"
						+ " WHERE id=? AND dodavatel<>? AND dostupne_od>=TO_DATE(?,'YYYY-MM-DD') AND dostupne_do>TO_DATE(?,'YYYY-MM-DD')  AND dostupne_od<=TO_DATE(?,'YYYY-MM-DD')";
				try (OraclePreparedStatement pstmtSelect2 = (OraclePreparedStatement) conn.prepareStatement("SELECT * FROM ARTICLE WHERE id=" + id + " for update")) {
						OracleResultSet rsx = (OracleResultSet) pstmtSelect2.executeQuery();
						if(rsx.next())
							id = rsx.getInt("id");
						try (OraclePreparedStatement pstmtUpdate = (OraclePreparedStatement) conn.prepareStatement(prikaz)) {
							pstmtUpdate.setInt(1, dodavatel);
							pstmtUpdate.setString(2, interval_od_str);
							pstmtUpdate.setString(3, interval_do_str);
							pstmtUpdate.setInt(4, id);
							pstmtUpdate.setInt(5, dodavatel);
							pstmtUpdate.setString(6, interval_od_str);
							pstmtUpdate.setString(7, interval_do_str);
							pstmtUpdate.setString(8, interval_od_str);

							pstmtUpdate.executeUpdate();
						}
					}
				}
				//conn.commit();
			}
		}
		/**
		 * Smaze artikl pro obdobi od do
		 *
		 * @param id Id produktu.
		 * @param interval_od Od kdy je dodávat.
		 * @param interval_do Do kdy je smluvně zavázán dodávat.
		 * @throws SQLException
		 */
	public static void smazArtikl(int id, Date interval_od, Date interval_do) throws SQLException {
		Connection conn = connection;
		if (connection.isClosed()) {
			conn = Databaze.Pripoj();
		}

		Date artikl_od = interval_od;
		Date artikl_do = interval_do;
		String interval_od_str = interval_od.toString();
		String interval_do_str = interval_do.toString();

		String prikaz = "SELECT * FROM article WHERE id=" + id + " ORDER BY 1";
		try (OraclePreparedStatement pstmtSelect = (OraclePreparedStatement) conn.prepareStatement(prikaz); OracleResultSet rset = (OracleResultSet) pstmtSelect.executeQuery()) {
			while (rset.next()) {
				artikl_od = rset.getDate("dostupne_od");
				artikl_do = rset.getDate("dostupne_do");
			}
		}

		if (artikl_od.after(interval_od) && artikl_do.before(interval_do)) {
			//interval cely v zadanem obdobi
			prikaz = "DELETE FROM article WHERE id=" + id
					+ " AND dostupne_od>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.before(interval_od) && artikl_do.after(interval_do)) {
			//interval presahujici zadane obdobi z obou stran
			prikaz = "INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)"
					+ "SELECT a.nazev, a.geometrie, a.typ, TO_DATE('" + interval_do_str
					+ "','YYYY-MM-DD') + 1/86400, a.dostupne_do,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel"
					+ "FROM article a WHERE a.id=" + id
					+ " AND a.dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);

			prikaz = "UPDATE article SET dostupne_do=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') - 1/86400"
					+ " WHERE id=" + id
					+ " AND dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.before(interval_od) && artikl_do.before(interval_do) && artikl_do.after(interval_od)) {
			//interval zleva delsi nez zadane obdobi
			prikaz = "UPDATE article SET dostupne_do=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') - 1/86400"
					+ " WHERE id=" + id
					+ " AND dostupne_od<TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD')  AND dostupne_do>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		} else if (artikl_od.after(interval_od) && artikl_do.after(interval_do) && artikl_od.before(interval_do)) {
			//interval zprava delsi nez zadane obdobi             
			prikaz = "UPDATE article SET dostupne_od=TO_DATE('" + interval_do_str + "','YYYY-MM-DD') + 1/86400 "
					+ " WHERE id=" + id
					+ " AND dostupne_od>=TO_DATE('" + interval_od_str + "','YYYY-MM-DD') AND dostupne_do>TO_DATE('" + interval_do_str + "','YYYY-MM-DD')  AND dostupne_od<=TO_DATE('" + interval_do_str + "','YYYY-MM-DD');";
			processStatement(conn, prikaz);
		}
	}
}
