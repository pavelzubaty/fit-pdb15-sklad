/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.projekt;

import pdb.gui.gui;

import java.io.IOException;
import static java.lang.System.exit;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;
import oracle.sql.ARRAY;
import oracle.sql.DATE;
import oracle.sql.ORAData;
import static pdb.gui.gui.detail_typ;
import static pdb.gui.gui.filterTypes;
import static pdb.projekt.Databaze.connection;


/**
 *
 * @author Radim Jílek, xjilek14
 */
public class main {

	private static final long serialVersionUID = 1L;
    private static final short maxX = 1200;
    private static final short maxY = 700;
    private static final short windowZoom = 1;
	
	
	/**
	 * @param args the command line arguments
	 * @throws java.io.IOException
	 */
	public static void main(String[] args) throws IOException {
		//JFrame frame = new JFrame();
		/*String login = System.getProperty("login");
		String passwd = System.getProperty("password");
        if( (login == null) || (null == passwd)) {
			gui.login = "xjilek14";
			gui.password = "itrug3uz";
		} else {
			gui.login = login;
			gui.password = passwd;
		}*/
		
		gui.dialog_prihlasovani.show();
		Databaze db;
		db = new Databaze(gui.login,gui.password);
		
		
		if(db != null && db.connection != null) {
			try {
				//Scanner scanner = new Scanner(System.in, "Windows-1250");
				//System.out.println("Založit datbázi? y - Ano, ostatni - Ne");
				//String answer = scanner.nextLine();
				boolean provedSkript;
				SQLReader sqlr = new SQLReader();
				String nazev_skriptu = "";
				if (gui.bl_createDB) {
					System.out.println("Zakládám databázi...");
					nazev_skriptu = "create-db.sql";
					provedSkript = sqlr.provedSkript("../src/main/resources/" + nazev_skriptu);
					if (!provedSkript) {
						System.err.println("Nepodařilo se provést skript: " + nazev_skriptu);
						Databaze.connection.close();
						exit(-1);
					}
					System.out.print("Inicializuji databázi");
					Databaze.initDb();
					nazev_skriptu = "showroom-insert-test.sql";
					provedSkript = sqlr.provedSkript("../src/main/resources/" + nazev_skriptu);
					if (!provedSkript) {
						System.err.println("Nepodařilo se provést skript: " + nazev_skriptu);
						Databaze.connection.close();
						exit(-1);
					}
				}
			} catch (SQLException ex) {
				//Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			System.out.println("Nepodařilo se připojit!");
			exit(-1);
		}
				
		
	}
	
}
