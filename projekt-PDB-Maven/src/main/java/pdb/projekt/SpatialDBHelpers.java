package pdb.projekt;


import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.spatial.geometry.JGeometry;
import pdb.gui.MapEntity;
import static pdb.projekt.Databaze.connection;

/**
 * Autor Robert Bikar, xbikar00 on 01.12.15.
 */
public class SpatialDBHelpers {

    /**
     *  Výjimka používáná pro detekci chyby - převod mezi JGeometry a Shapem
     */
    public class JGeometry2ShapeException extends Exception {
    }

    ;
    private java.util.List<MapEntity> mapEntities;


    /**
     * Konstruktor pro dbHelpers
     * Incializuje seznam mapEntities
     */
    public SpatialDBHelpers() {
        mapEntities = new ArrayList<>();

    }

    /**
     * Nahraje prostorové objekty z DB z tabulky showroom
     * Vytváří objekty zobrazitelné v mapě - tj. převed JGeometry na Shape a vytvoří MapEntity
     *
     * @throws Exception
     */
    public void loadShapesFromDb() throws Exception {
        Connection conn = connection;
        if (connection.isClosed())
            conn = Databaze.Pripoj();

        Statement stmt = conn.createStatement();
        {
            try (ResultSet resultSet
                         = stmt.executeQuery("select id, nazev, geometrie, idArticle from showroom")) {
                while (resultSet.next()) {

                    byte[] image = resultSet.getBytes("geometrie");
                    JGeometry jGeometry = JGeometry.load(image);
                    Shape shape = jGeometry2Shape(jGeometry);


                    if (shape != null) {
                        mapEntities.add(new MapEntity(shape, jGeometry, resultSet.getByte("id"), resultSet.getByte("idArticle")));
                    }
                }
            }
        }

        //conn.commit();
        conn.close();


    }


    /**
     * Vytváří objekt Shape z JGeometriy
     *
     * @param jGeometry vstupní jGeomtry objekt
     * @return převedený odpovídají Shape objekt
     * @throws JGeometry2ShapeException pokud není možno převést na SHape
     */
    public Shape jGeometry2Shape(JGeometry jGeometry) throws JGeometry2ShapeException {
        Shape shape;

        switch (jGeometry.getType()) {

            case JGeometry.GTYPE_POLYGON:
            case JGeometry.GTYPE_CURVE:
            case JGeometry.GTYPE_COLLECTION:
            case JGeometry.GTYPE_MULTICURVE:
                shape = jGeometry.createShape();
                break;

            default:
                throw new JGeometry2ShapeException();
        }
        return shape;
    }


    public java.util.List<MapEntity> getShapes() {
        return mapEntities;
    }

    /**
     * Metoda provádí update souřadnic daného objektu v db
     *
     * @param entity vstupní objekt mapy
     */
    public void updateMapEntity(MapEntity entity) {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            try (PreparedStatement pstmt = conn.prepareStatement(
                    " update SHOWROOM set geometrie=? where ID=? ")) {
                Struct obj = JGeometry.storeJS(conn, entity.getTransjGeom());
                pstmt.setObject(1, obj);
                pstmt.setInt(2, entity.getId());
                pstmt.executeUpdate();
                System.err.println("DBupdated");

            } catch (Exception e) {
                System.err.println(e.getMessage());

            }
            //conn.commit();
            conn.close();


        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }

    }

    /**
     * Metoda provádí smazaní prostorového objektu v db.
     *
     * @param mapEntityId id objektu na smazání
     */
    public void deleteEntity(int mapEntityId) {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            try (PreparedStatement pstmt = conn.prepareStatement(
                    " DELETE from SHOWROOM where ID=? ")) {
                pstmt.setInt(1, mapEntityId);
                pstmt.executeUpdate();

            } catch (Exception e) {
                System.err.println(e.getMessage());

            } finally {
                System.err.println("Entity" + mapEntityId + "deleted");

            }
            //conn.commit();
            conn.close();


        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }
    }

    /**
     * Metoda provádí vložení objektu do databáze
     * do tabulky showroom
     *
     * @param entity vstupní objekt
     */
    public void insertEntity(MapEntity entity) {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            try (PreparedStatement pstmt = conn.prepareStatement(
                    "INSERT INTO showroom(nazev, geometrie, idArticle) VALUES( ? , ?, ?)")) {

                pstmt.setString(1, "zidleNew");
                Struct obj = JGeometry.storeJS(conn, entity.getjGeom());
                pstmt.setObject(2, obj);
                pstmt.setInt(3, entity.getArticleId());
                pstmt.executeUpdate();

            } catch (Exception e) {
                System.err.println(e.getMessage());

            } finally {
                System.err.println("Entity" + "inserted");

            }
            //conn.commit();
            conn.close();


        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }

    }

    /**
     * Vytvoření pohledu z tabuek article a showroom
     *
     */
    private void createViewArticleShowroom()
    {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (PreparedStatement pstmt = conn.prepareStatement(
                "  CREATE OR REPLACE VIEW article_showroom AS " +
                        "SELECT showroom.id, showroom.geometrie,ARTICLE.TYP" +
                        "        from showroom" +
                        "        INNER JOIN ARTICLE" +
                        "        on showroom.idArticle = article.id")) {

            pstmt.executeUpdate();

        } catch (Exception e) {
            System.err.println(e.getMessage());

        } finally {
            System.err.println("View article_showroom created");

        }
    }

    /**
     * Prostorový dotaz pro zjištění plochy
     * všech stolů, žídlí a celkové plochy stolů a židlí
     *
     * @return
     */
    public double[] spatialQuery1() {
        createViewArticleShowroom();

        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }


        double []areas = {0,0,0,0,0};

        try {
            Statement stmt = conn.createStatement();
            {
                try (ResultSet resultSet
                             = stmt.executeQuery("SELECT SUM(SDO_GEOM.SDO_AREA(geometrie, 1)) plocha " +
                        "FROM article_showroom " +
                        "WHERE typ LIKE 'Židle' "+
                        "UNION " +
                        "SELECT SUM(SDO_GEOM.SDO_AREA(geometrie, 1)) plocha " +
                        "FROM article_showroom " +
                        "WHERE typ LIKE 'Stůl' " +
                        "UNION " +
                        "SELECT SUM(SDO_GEOM.SDO_AREA(geometrie, 1)) plocha " +
                        "FROM article_showroom " +
                        "WHERE typ LIKE 'Stůl s dírou' "



                )) {
                    int index = 0;
                    while (resultSet.next()) {
                        areas[index] = resultSet.getDouble(1);

                        index++;
                    }
                }
            }

            //conn.commit();
            //conn.close();
        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }



        try {
            Statement stmt = conn.createStatement();
            {
                try (ResultSet resultSet
                             = stmt.executeQuery("SELECT SUM(SDO_GEOM.SDO_AREA(geometrie, 1)) plocha " +
                        "FROM article_showroom " +
                        "WHERE typ LIKE 'Židle' OR typ LIKE 'Stůl' OR typ LIKE 'Stůl s dírou'")) {

                    while (resultSet.next()) {
                        areas[3] = resultSet.getDouble(1);


                    }
                }
            }
            conn.close();
        }
        catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }







        return areas;
    }


    /**
     * Prostorový dotaz pro zjištění konvexních obálek
     * jednotlivých typů objektů a konvexní obálky všech objektů
     *
     *
     * @return vektor objektů Shape reprezentující konvexní obálky
     */
    public Vector<Shape> spatialQuery2() {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }


        Vector<Shape> shapes = new Vector<>();
        try {

            try (PreparedStatement pstmt1 = conn.prepareStatement(
                    "SELECT SDO_AGGR_CONVEXHULL( " +
                            "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                            "FROM article_showroom a " +
                            "WHERE typ LIKE 'Křeslo' "


            )) {
                ResultSet resultSet = pstmt1.executeQuery();

                resultSet.next();
                byte[] image = resultSet.getBytes(1);
                JGeometry jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                PreparedStatement pstmt3 = conn.prepareStatement(
                        "SELECT SDO_AGGR_CONVEXHULL( " +
                                "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                                "FROM article_showroom a " +
                                "WHERE typ LIKE 'Stůl' ");

                resultSet = pstmt3.executeQuery();
                resultSet.next();
                image = resultSet.getBytes(1);
                jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                PreparedStatement pstmt4 = conn.prepareStatement(
                        "SELECT SDO_AGGR_CONVEXHULL( " +
                                "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                                "FROM article_showroom a " +
                                "WHERE typ LIKE 'Stůl s dírou' ");

                resultSet = pstmt4.executeQuery();
                resultSet.next();
                image = resultSet.getBytes(1);
                jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                PreparedStatement pstmt5 = conn.prepareStatement(
                        "SELECT SDO_AGGR_CONVEXHULL( " +
                                "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                                "FROM article_showroom a " +
                                "WHERE typ LIKE 'Gauč' ");

                resultSet = pstmt5.executeQuery();
                resultSet.next();
                image = resultSet.getBytes(1);
                jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                PreparedStatement pstmt6 = conn.prepareStatement(
                        "SELECT SDO_AGGR_CONVEXHULL( " +
                                "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                                "FROM article_showroom a " +
                                "WHERE typ LIKE 'Židle' ");

                resultSet = pstmt6.executeQuery();
                resultSet.next();
                image = resultSet.getBytes(1);
                jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                PreparedStatement pstmt7 = conn.prepareStatement(
                        "SELECT SDO_AGGR_CONVEXHULL( " +
                                "    SDOAGGRTYPE(a.geometrie, 0.05)) " +
                                "FROM article_showroom a "
                               );

                resultSet = pstmt7.executeQuery();
                resultSet.next();
                image = resultSet.getBytes(1);
                jGeometry = JGeometry.load(image);
                shapes.add(jGeometry2Shape(jGeometry));

                resultSet.close();
            }
            catch (Exception e )
            {
                System.err.println(e.getMessage());
            }



            conn.close();
        }
        catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }


        return  shapes;
    }

    /**
     * Prostorový dotaz na získání nejbližší objektů
     * k označenému objektu na mapě
     *
     * @param n počer hledaných objektů
     * @param selectedEntity  vstupní objekt z mapy
     * @return vektor objektů shape reprezentující nejlbižší objekty
     * */
    public Vector<Integer> spatialQuery3(MapEntity selectedEntity, int n) {

        createViewArticleShowroom();
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }


        Vector<Integer> v = new Vector<>();

        try {

                try (PreparedStatement pstmt = conn.prepareStatement("SELECT /*+ INDEX(i1 a_spatial_idx) */ " +
                        "a.id  FROM article_showroom a " +
                        "WHERE SDO_NN(a.geometrie, " +
                        " (SELECT a.geometrie FROM article_showroom a where id = ?) , " +
                        " ?  ) = 'TRUE'")) {

                    pstmt.setInt(1, selectedEntity.getId());
                    String s = "sdo_num_res=";
                    s = s.concat(Integer.toString(n));
                    pstmt.setNString(2, s);

                    ResultSet resultSet = pstmt.executeQuery();

                    while (resultSet.next()) {
                        v.add(resultSet.getInt(1));

                    }

                    resultSet.close();
                }
                catch (Exception e )
                {
                    System.err.println(e.getMessage());
                }



            conn.close();
        }
        catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }



        return v;
    }

    /**
     * Prostorový dotaz na zjištění průnik jakýchkoli
     * dvou prostorových objektů.
     *
     *
     * @return vektor objektů Shape reprezentující průniky objektů
     */
    public Vector<Shape> spatialQuery4() {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        Vector<Shape> intersections = new Vector<>() ;

        try {

            try (PreparedStatement pstmt = conn.prepareStatement(
                    " SELECT SDO_GEOM.SDO_INTERSECTION(g.aa, g.bb, 0.005) " +
                    " FROM " +
                    "         (SELECT  A.GEOMETRIE aa, B.GEOMETRIE bb " +
                    "                 FROM SHOWROOM A, SHOWROOM B "+
                     "               WHERE A.ID != B.ID "+
                      "              AND SDO_RELATE(A.GEOMETRIE, B.GEOMETRIE, "+
                      "              'mask=ANYINTERACT') = 'TRUE') g" )) {

                ResultSet resultSet = pstmt.executeQuery();

                while (resultSet.next()) {
                    byte[] image = resultSet.getBytes(1);
                    JGeometry jGeometry = JGeometry.load(image);
                    intersections.add(jGeometry2Shape(jGeometry));

                }

                resultSet.close();
            }
            catch (Exception e )
            {
                System.err.println(e.getMessage());
            }



            conn.close();
        }
        catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }


        return intersections;
    }


    /**
     * Metoda všchny objekty v tabulce showroom
     *
     *
     */
    public void deleteAllEntities() {
        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            try (PreparedStatement pstmt = conn.prepareStatement(
                    " DELETE from SHOWROOM ")) {

                pstmt.execute();

            } catch (Exception e) {
                System.err.println(e.getMessage());

            } finally {
                System.err.println("All map entities were deleted");

            }
            //conn.commit();
            conn.close();


        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }

    }

    /**
     * Metoda získá maximální id v tabulce showroom
     *
     *
     *
     * @return maximální id
     */
    public int getHighestIdFromShowroom() {

        Connection conn = connection;
        try {
            if (connection.isClosed())
                conn = Databaze.Pripoj();
        } catch (SQLException ex) {
            Logger.getLogger(SpatialDBHelpers.class.getName()).log(Level.SEVERE, null, ex);
        }

        int maxId = 0;

        try {
            Statement stmt = conn.createStatement();
            {
                try (ResultSet resultSet
                             = stmt.executeQuery("select MAX(ID) from showroom")) {
                    while (resultSet.next()) {
                        maxId = resultSet.getInt(1);


                    }
                }
            }

            //conn.commit();
            conn.close();
        } catch (SQLException sqlE) {
            System.err.println(sqlE.getMessage());
        }
        return maxId;
    }
}