package pdb.projekt;

import oracle.spatial.geometry.JGeometry;
import pdb.gui.MapEntity;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Autor Robert Bikar, xbikar00 on 03.12.15.
 *
 * Třída pro vytváření geometrických tvarů
 * Dle zadého parametru  vrací příslušný tvar
 *
 */
public class GeometryFactory {

    private static final int POLYGON2D = 2003;
    private static final int LINESTRING = 2002;
    private static final int COLLECTION = 2004;
    private static final int MULTICURVE = 2006;

    /**
     * Enum type jednotliých typů geometrií
     *
     *
     *
     */
    public  enum GeometryType {CHAIR, TABLE, TABLEWITHHOLE, SOFA, ARMCHAIR;


        public static GeometryType fromInteger(int x) {
            switch(x) {
                case 0:
                    return CHAIR;
                case 1:
                    return TABLE;
                case 2:
                    return TABLEWITHHOLE;
                case 3:
                    return SOFA;
                case 4:
                    return ARMCHAIR;
            }
            return null;
        }

    }


    /**
     * Kontruktor pro vytváření jednotliých geomtrých dle parametrů
     *
     * @param type typ geometrie
     * @return geometrie
     */
    public JGeometry createJGeom(GeometryFactory.GeometryType type)
    {

        JGeometry jGeom = null;
        switch (type)
        {
            case TABLE:
                jGeom = createTable();
                break;
            case CHAIR:
                jGeom = createChair();
                break;
            case SOFA:
                jGeom = createSofa();
                break;
            case TABLEWITHHOLE:
                jGeom = createTableWithHole();
                break;
            case ARMCHAIR:
                jGeom = createArmChair();
                break;
            default:
                System.err.println("Unsupported geometry" + type);
                break;
        }

        return jGeom;


    }

    /**
     * Vytvoří geomtrii židle
     * Jedná se o obyčejný polygon
     * @return geometrie židle
     */
    private JGeometry createChair()
    {
        int[] elemInfo = {1, 1005, 2, 1,2,1, 7,2,2};
        double[] ordinates = {10,50, 10,10, 70,10, 70,50, 35,60, 10, 50};
        adjustOrdinates(ordinates, 10.0, 10.0);
        return new JGeometry(POLYGON2D, 0, 0, 0, 0, elemInfo, ordinates);

    }


    /**
     * Vytvoří geometrii stolu s dírou
     * Jedná se o interior a ext. polygon
     *
     * @return geometrie stolu s dírou
     */
    private JGeometry createTableWithHole()
    {


        double c = 100;
        int[] elemInfo = {1,1003,4, 7,2003,4 };
        double[] ordinates = {100,100, 150,150, 200,100,
                            160,100, 150,110, 140,100} ;
        adjustOrdinates(ordinates, 100.0,50.0    );
        return new JGeometry(POLYGON2D, 0, 0, 0, 0, elemInfo, ordinates);



    }


    /**
     * Vytvoří geometrii pohovky
     * Jedná se o linestring
     *
     * @return geometrie pohovky
     */
    private JGeometry createSofa()
    {


        double c = 300;
        int[] elemInfo = {1,4,7, 1,2,1, 3,2,1, 5,2,2, 9,2,1, 11,2,1, 13,2,1, 15,2,1};
        double[] ordinates = {100,100, 100,160,
                                180,160,
                                225, 130, 240,100,
                                240, 20,
                                180,20,
                                180, 100,
                                100, 100
                                };
        adjustOrdinates(ordinates,100.0,20.0);
        return new JGeometry(LINESTRING, 0, 0, 0, 0, elemInfo, ordinates);



    }

    /**
     * Vytvoří geometrii stolu
     * Jedná se o kolekci
     * @return geometrie stolu
     */
    private JGeometry createTable()
    {

        int[] elemInfo = {1,1003,1, 11,4,4, 11,2,2, 15,2,1, 17,2,1, 19,2,1, 23,1003,1  };
        double[] ordinates = {20,20, 80,20, 80,120, 20,120, 20,20,
                                80,100, 120,80, 160,100,
                                160,20,
                                80,20,
                                80,100,
                                240,20, 240,120, 160,120, 160,20};
        adjustOrdinates(ordinates,20.0, 20.0);
        return new JGeometry(COLLECTION, 0, 0, 0, 0, elemInfo, ordinates);


    }

    /**
     * Vytvoří geometrii křesla
     * Jedná se typ multicurve
     *
     * @return geometrie křesla
     */
    private JGeometry createArmChair()
    {
        int[] elemInfo = {1,2,1, 11,2,1, 15,2,1, 19,4,2, 19,2,1, 21,2,2 };
        double[] ordinates = {100, 100, 100, 150, 160, 140, 160, 110, 100, 100,
                            160,118, 170,118,
                            160, 132, 170, 132,
                              170,110, 170, 140,
                             175,125, 170,110  };

        adjustOrdinates(ordinates, 100.0, 100.0);
        return new JGeometry(MULTICURVE, 0, 0, 0, 0, elemInfo, ordinates);


    }



    private void adjustOrdinates(double[] ordinates, double x, double y)
    {
        for (int i=0; i<ordinates.length;i+=2)
        {
            ordinates[i] = ordinates[i]-x;
            ordinates[i+1] = ordinates[i+1]-y;
        }

    }

    /**
     * Vytvoří obrázek dané geometrie
     *
     * @param type typ geometrie
     * @return obrázek geometrie
     */
    public static  BufferedImage createImageFromGeometry (GeometryType type)
    {

        SpatialDBHelpers db = new SpatialDBHelpers();
        Shape s = new Rectangle(0,0,0,0);
        try {
            s = db.jGeometry2Shape(new GeometryFactory().createJGeom(type));
        }

        catch(Exception e)
        {
            System.err.println("unable to convert jgeometry to shape\n" +  e.getMessage());
        }



        BufferedImage img = new BufferedImage((int)s.getBounds2D().getWidth(), (int)s.getBounds2D().getHeight(), BufferedImage.TYPE_4BYTE_ABGR_PRE);
		
		
        Graphics2D g = img.createGraphics();
		g.setBackground(Color.lightGray);
        g.setPaint(Color.BLUE);
        g.fill(s);
        //g.setColor(Color.BLUE);
        //g.draw(s);
        //g.dispose();
       // JOptionPane.showMessageDialog(null,img);
		return img;
/*
        BufferedImage scaledImg = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
        g = scaledImg.createGraphics();
        g.drawImage(img, 0, 0, 50, 50, null);
        g.dispose();

        return scaledImg;*/
    }











}
