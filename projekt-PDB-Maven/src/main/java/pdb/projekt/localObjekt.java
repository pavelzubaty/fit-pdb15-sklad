/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.projekt;

import java.sql.Date;
import oracle.ord.im.OrdImage;

/**
 * Uchovává informace o objektu na mapě
 * 
 * @author Radim Jílek, xjilek14
 */
public class localObjekt {
    private final int id;
    private final String typ;
	private final OrdImage nahled;
    private final String nazev;
	private final String popis;
	private final int geometrie;
	private final Date time_od;
	private final Date time_do;
	private final String dodavatel;
    
    public localObjekt(int id,String typ, String nazev, String popis, int geometrie, Date time_od, Date time_do, String dodavatel, OrdImage nahled) {
        this.typ = typ;
        this.popis = popis;
        this.nazev = nazev;
        this.id = id;
		this.geometrie = geometrie;
		this.time_do = time_do;
		this.time_od = time_od;
		this.dodavatel = dodavatel;
		this.nahled = nahled;
    }

	public OrdImage getNahled() {
		return nahled;
	}

	public Date getTime_od() {
		return time_od;
	}

	public Date getTime_do() {
		return time_do;
	}

	public int getId() {
		return id;
	}

	public String getTyp() {
		return typ;
	}

	public String getNazev() {
		return nazev;
	}

	public String getPopis() {
		return popis;
	}

	public int getGeometrie() {
		return geometrie;
	}

	public String getDodavatel() {
		return dodavatel;
	}
	
	@Override
	public String toString() {
		String obj = ""+nazev+", "+typ+" (id="+id+", "+dodavatel+"):\n"+"\t"+time_od+" - "+time_do+"\n\t"+popis;
		return obj;
	}
    
    
}


