/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Radim Jílek, xjilek14
 */
class MenuToolbarListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Tlacitko menu: "+e.getActionCommand());
		switch(Integer.parseInt(e.getActionCommand())) {
			case(1):
				((ObjectMap)gui.mapa).showAreaOfTablesAndChairs();
				break;
			case(2):
				((ObjectMap)gui.mapa).findConvexHull();
				break;
			case(3):
				int countNum = 0;
				String count = JOptionPane.showInputDialog(null , "Vložte počet hledaných sousedících objektů");
				try{
					countNum = Integer.parseInt(count);
					((ObjectMap)gui.mapa).findNearestNeighbors(countNum + 1);
				}
				catch (NumberFormatException exc)
				{
					JOptionPane.showMessageDialog(null, "Nebylo zadáno číslo", "Chyba vstupu", JOptionPane.ERROR_MESSAGE);
				}


				break;
			case(4):





				break;
			case(5):
				((ObjectMap)gui.mapa).checkOverlaps();
				break;
			case(6):
				break;
		}
	}
	
}
