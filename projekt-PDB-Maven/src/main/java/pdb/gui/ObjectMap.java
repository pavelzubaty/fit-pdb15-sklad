package pdb.gui;



import oracle.spatial.geometry.JGeometry;
import pdb.projekt.GeometryFactory;
import pdb.projekt.SpatialDBHelpers;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.sql.SQLException;
import java.util.*;
/**
 *Autor: Robert Bikar, xbikar00 on 25.11.15.
 * Třídá se stará o vykreslování a práci s objekty na mapě
 * Je možné s objekty hýbat tažením myšky, označovat, otáčet, zvětšovat a mazat je.
 * Změny souřadnic objektů jsou ukládány do databáze.
 */
public class ObjectMap extends  JPanel {



    /**
     * Třída definuje pop up menu, které se zobrazí
     * po kliknutí pravým tlačítkem myši.
     * Pop up menu umožňuje zobrazit detailní informace o objektu
     * a mazat ho.
     */
    class EntityPopUpMenu extends JPopupMenu {
        public JMenuItem showDetailsMenuItem;
        public JMenuItem deleteEntitymenuItem;
        public EntityPopUpMenu(){

            deleteEntitymenuItem = new JMenuItem("Smazat objekt");
            if (currentEntity != null) {
                showDetailsMenuItem = new JMenuItem("Ukázat informace - artikl: " + currentEntity.getArticleId());

            }
            else
            {
                showDetailsMenuItem = new JMenuItem("Ukázat informace - artikl:");
                showDetailsMenuItem.setEnabled(false);
                deleteEntitymenuItem.setEnabled(false);
            }


            showDetailsMenuItem.addActionListener(new MyListener());
            deleteEntitymenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteCurrentEntity();
                }
            });
            add(showDetailsMenuItem);
            add(deleteEntitymenuItem);

        }
    }

    /**
     * Třída listeneru pro Pop up menu
     */
    class PopClickListener extends MouseAdapter {
        public void mousePressed(MouseEvent e){
            if (e.isPopupTrigger())
                doPop(e);
        }

        public void mouseReleased(MouseEvent e){
            if (e.isPopupTrigger())
                doPop(e);
        }

        private void doPop(MouseEvent e){
            EntityPopUpMenu menu = new EntityPopUpMenu();
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    private Vector<Shape> convexHulls;
    private Vector<Shape> intersects;
    //private boolean overlap = false;
    Vector<Integer> entitiesToSelectFromSpatialQuery;

    private java.util.List<MapEntity> entities = new ArrayList<>();
    private MapEntity currentEntity;

    private double lastOffsetX;
    private double lastOffsetY;

    private static double minX = 0.0;
    private static double maxX = 800.0;
    private static double minY = 0.0;
    private static double maxY = 600.0;
    private boolean addingNewEntity = false;
    SpatialDBHelpers dbHelpers;

    /**
     * Kostruktor mapy prostorových objektů
     * Přidáváná listenery pro myš a klávesnici
     */
    public ObjectMap() {
        addMouseListener(new PopClickListener());
        entitiesToSelectFromSpatialQuery = new Vector<>();
        convexHulls = new Vector<>();
        intersects = new Vector<>();
        setBorder(BorderFactory.createLineBorder(Color.black));
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_DELETE || keyCode == KeyEvent.VK_BACK_SPACE)
                {
                    deleteCurrentEntity();
                }
            }
        });

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {


                resetColorsOfAllEntities();
                entitiesToSelectFromSpatialQuery.clear();
                convexHulls.clear();
                intersects.clear();
               // if (SwingUtilities.isLeftMouseButton(e))
                {

                    lastOffsetX = e.getPoint().getX();
                    lastOffsetY = e.getPoint().getY();


                    if (addingNewEntity)
                    {

                        addingNewEntity = false;
                        currentEntity.setIsSelected(true);
                        dbHelpers.insertEntity(currentEntity);
                        currentEntity.setId(dbHelpers.getHighestIdFromShowroom());

                        repaint();

                    }
                    else
                    {
                        selectEntity(e);
                    }



                }

            }
        });

        addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent e) {
                moveEntity(e.getPoint().getX(),e.getPoint().getY());



            }
        });
        addMouseMotionListener(new MouseAdapter(){
            @Override
            public  void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);

                if (addingNewEntity)
                {

                    moveEntity(e.getX(),e.getY());
                }


            }




        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (currentEntity != null)
                {
                    currentEntity.isDragged = false;
                    currentEntity.setRotated(false);
                    currentEntity.setScaled(false);

                }

                repaint();
            }
        });

        dbHelpers = new SpatialDBHelpers();

    }

    /**
     * Mění souřadnice prostorového objektu na základě
     * daných souřadnic x, y a na základě varianty změny souřadnice: rotace, zvětšení/zmenšení, dragging
     * @param x souřadnice x
     * @param y souřadnice y
     */
    private void moveEntity(double x, double y) {

        if (currentEntity == null) return;


        if (currentEntity.isRotated())
        {
            currentEntity.setPositionChanged(true);
            double moveX =  x - lastOffsetX;
            double moveY =  y - lastOffsetY;
            double angle = Math.atan2(moveY - y, moveX - x);
            angle -= Math.PI/2.0;
            angle = Math.toDegrees(angle);
            //if (angle < 0) {
              //  angle += 360;
           // }
            currentEntity.setRotationAngle(angle);
            lastOffsetX += moveX;
            lastOffsetY += moveY;


           // System.out.println("should now be rotating  " + angle);
        }
        else if (currentEntity.isScaled())
        {
            currentEntity.setPositionChanged(true);
           // double moveX =  x - lastOffsetX;
            //double moveY =  y - lastOffsety;
            double scaleX =  1;
            double scaleY =  1;


            scaleX = x / lastOffsetX;
            scaleY = y / lastOffsetY;
            if (lastOffsetX <= x && lastOffsetY <= y) {



            } else if (lastOffsetX <= x && lastOffsetY >= y) {

                //scaleX *= -1;
            } else if (lastOffsetX >= x && lastOffsetY >= y) {
                //scaleX *= -1;
                //scaleY *= -1;
            } else if (lastOffsetX >= x && lastOffsetY <= y) {
                //scaleX *= -1;

            }

            currentEntity.setScaleX(scaleX);
            currentEntity.setScaleY(scaleY);
            //currentEntity.translateX = x - lastOffsetX;
            //currentEntity.translateY = y - lastOffsetY;
           // lastOffsetX += moveX;
          //  lastOffsetY += moveY;

        }
        else
        {
            currentEntity.isDragged =true;
            currentEntity.setPositionChanged(true);

            if (x < minX) x = minX;
            if (x > maxX) x = maxX;
            if (y < minY) y = minY;
            if (y > maxY) y = maxY;


            double moveX =  x - lastOffsetX;
            double moveY =  y - lastOffsetY;

            currentEntity.translateX += moveX;
            currentEntity.translateY += moveY;
            lastOffsetX += moveX;
            lastOffsetY += moveY;
        }



           // currentEntity.getTransformedShape().getBounds().setLocation(currentEntity.x, currentEntity.y);
         //  System.out.println(currentEntity.getTransformedShape().getBounds().x + " " + currentEntity.y + " " + x + " " + y);
            repaint();
           // currentEntity.isDragged = false;
       // }
    }

    /**
     * Stará se o výběr objektu na mapě.
     * Pokud se po kurzorem myši po klinutí nachází objekt
     * je nastaven jako aktuální objekt (je označen), změní se mu barva
     * objeví se pomocné objekty pro rotaci a zvěšování.
     * @param e
     */
    private void selectEntity(MouseEvent e)
    {
        boolean entityFound = false;
        //currentEntity = null;
        Point p = e.getPoint();
        //apEntity resultEntity = null;
        for (MapEntity entity : this.entities) {
            if (entity.getTransformedShape().contains(p)) {
                if (!entityFound)
                {
                    currentEntity = entity;
                    currentEntity.setIsSelected(true);
                    requestFocus();

                    //currentEntity = null;
                    //break;
                    entityFound = true;
                }


            }
            else
            {
                entity.setIsSelected(false);
                entity.setColor(Color.BLACK);
            }
        }

        if (!entityFound && currentEntity!=null)
        {

            if (findRotationEnabler(p))
            {
                currentEntity.setIsSelected(true);
                currentEntity.setRotated(true);

            }
            else if(findScalingEnabler(p))
            {
                currentEntity.setScaled(true);
                currentEntity.setIsSelected(true);
            }
            else{
                currentEntity = null;
            }



        }


        repaint();

    }

    /**
     * Vrací velikost mapy
     * @return velikot mapy
     */
    public Dimension getPreferredSize() {
        return new Dimension(800,600);
    }

    /**
     * Metoda pro vykreslování objektů
     * Postupně vykresluje všechny prostorové objekty
     * Pokud jsou k dispozici knovexní obálky, tak se vykreslí i ty.
     * Volá metody pro update souřadnic objektu v db.
     * @param g
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);



        for (MapEntity entity: this.entities)
        {
            for (int id : entitiesToSelectFromSpatialQuery) {

                if (entity.getId() == id) {
                    entity.setColor(Color.magenta);
                }

            }



            if (entity.isPositionChanged() && !entity.isDragged && !entity.isRotated() && !entity.isScaled())
            {
                entity.updateJGeom();
                dbHelpers.updateMapEntity(entity);
                entity.setPositionChanged(false);
                checkOverlaps();
             }
            if (!entity.equals(currentEntity))
                entity.paintEntity(g);

        }

        if (currentEntity != null)
            currentEntity.paintEntity(g);

        if (!convexHulls.isEmpty())
        {
            Graphics2D gg = (Graphics2D) g;
            Random rand = new Random();
            for (Shape s: convexHulls) {


                float red = rand.nextFloat();
                float green = rand.nextFloat();
                float blue = rand.nextFloat();
                gg.setColor(new Color(red, green, blue));
                gg.draw(s);
            }


        }


        if  (!intersects.isEmpty())
        {
            for (Shape s: intersects)
            {
                Graphics2D gg = (Graphics2D) g;
                gg.setPaint(Color.RED);
                gg.setStroke(new BasicStroke(3));
                gg.fill(s);

            }


        }


    }




    private boolean findRotationEnabler(Point p)
    {
        return currentEntity.getRotationShape().contains(p);
    }

    private boolean findScalingEnabler(Point p)
    {
        return currentEntity.getScalingShape().contains(p);
    }

    /**
     * Prida objekt do mapy. Vytvari se novy objekt MapEntity dle zadaného
     * typu gemoetrie.
     * Nová pozice objektu je určena tahem myší a uloží se na první kliknutí.
     *
     * @param geometryType typ geometrie
     * @param idArtikl id artiklu z tabulky aticles
     */
    public void addMapEntityToMap(GeometryFactory.GeometryType geometryType, int idArtikl)
    {
        if (currentEntity != null)
            currentEntity.setIsSelected(false);

        addingNewEntity = true;
        JGeometry jGeom = new GeometryFactory().createJGeom(geometryType);
        MapEntity newMapEntity;
        try {
            newMapEntity = new MapEntity(dbHelpers.jGeometry2Shape(jGeom), jGeom, 0, idArtikl);
            //newMapEntity.translateX = this.getWidth()/2.0;
            //newMapEntity.translateY =  this.getHeight()/2.0 - 40.0;
            entities.add(newMapEntity);
            currentEntity = newMapEntity;
            currentEntity.setIsSelected(true);
            lastOffsetX =0;
            lastOffsetY =0;
            currentEntity.translateX=0;
            currentEntity.translateY=0;

           // dbHelpers.insertEntity(newMapEntity);
        }
        catch (SpatialDBHelpers.JGeometry2ShapeException e)
        {
            System.err.println("unable to create shape from jgeom " + e.getMessage());
        }



        try {


            Robot robot = new Robot();
            robot.mouseMove((int)this.getLocationOnScreen().getX() + this.getWidth()/2, (int)this.getLocationOnScreen().getY() + this.getHeight()/2 );

        }
        catch (AWTException e)
        {
            System.err.println("unable to move with mouse " + e.getMessage());
        }

   //     repaint();



    }


    /**
     * Vymaže všechny objekty z mapy a zavolá metodu pro vymáazání obsahu tabulky v db.
     */
    public void deleteAllMapEntities()
    {
        entities.clear();
        dbHelpers.deleteAllEntities();
        repaint();
    }

    /**
     * Metoda pro inicializaci mapy.
     * Nahraje z databáze aktuální obsah mapy
     * a vykreslí získané objekty.
     */
    public void initMap()
    {

        try{


            dbHelpers.loadShapesFromDb();
            this.entities = dbHelpers.getShapes();

        }
        catch (SQLException e)
        {
            System.err.println("SQL Exception: Unable to load shapes from db. " + e.getCause());
        }
        catch (Exception e)
        {
            System.err.println("General error occured while loading shapes: " + e.getMessage());
        }
        repaint();

    }

    /**
     * Volá prostorový dotaz na plochu stolů a židlí
     * Zobrazuje výsledek v dialogu
     *
     */
    public void showAreaOfTablesAndChairs()
    {

        double areas[] = dbHelpers.spatialQuery1();
        double areaTables = areas[1]  + areas[2];

        long areaTablesRounded = Math.round(areaTables);
        long areaChairs = Math.round(areas[0]);
        long areaChairsTables = Math.round(areas[3]);

        JOptionPane.showMessageDialog(null, "Zastavěná plocha stoly: " +  areaTablesRounded
                                    +  "\nZastavěná plocha židlemi: " +  areaChairs
                                    + "\nZastavěná plocha celkem (stoly + židle):" + areaChairsTables


        );


    }


    private void deleteCurrentEntity()
    {
        if (currentEntity != null)
        {

            dbHelpers.deleteEntity(currentEntity.getId());
            entities.remove(currentEntity);
            currentEntity = null;
            repaint();
        }
    }

    /**
     * Volá prostoorvý dotaz na zjištění nejbližších objektů
     *
     * @param n počet objektů, které se mají nalézt
     */
    public void findNearestNeighbors(int n)
    {


        if (currentEntity != null)
        {
            entitiesToSelectFromSpatialQuery = dbHelpers.spatialQuery3(currentEntity, n);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Není označen objekt v mapě");

        }
        repaint();

    }


    private void resetColorsOfAllEntities()
    {

        for (MapEntity entity: entities)
        {
            entity.setColor(Color.black);
        }
    }

    /**
     * Volá prostorový dotaz na zjištění
     * konvexních obálek jednotlivých typů objektů
     *
     *
     */
    public void findConvexHull()
    {

        convexHulls= dbHelpers.spatialQuery2();

        repaint();

    }

    /**
     * Volá prostorový dotaz na kotrolu překryvů
     *
     */
    public void checkOverlaps()
    {
        intersects = dbHelpers.spatialQuery4();
        boolean isIntersect = false;
        if (!intersects.isEmpty())
        {
           isIntersect = true;

                JOptionPane.showMessageDialog(null,
                        "Byly nalezeny překryvy nábytku, toto umístění není validní!", "Chyba",  JOptionPane.ERROR_MESSAGE);
            repaint();

        }
        else
        {
            isIntersect = false;
        }


        repaint();

    }


}
