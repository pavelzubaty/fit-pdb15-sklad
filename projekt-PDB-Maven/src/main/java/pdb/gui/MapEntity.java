package pdb.gui;

/**
 * Autor: Robert Bikar, xbikar00 on 25.11.15.
 *
 * Třída reprezentuje objekt v mapě
 * Poskytuje metody pro kvykreslení objektu,
 * transformace - rotace, posunutí, zvětšení.
 *
 */

import oracle.spatial.geometry.JGeometry;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

public class MapEntity {
    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    private int articleId;
    private Color color;
    private boolean isSelected = false;
    private Shape transformedShape;
    private Shape originalShape;
    private Rectangle rect;
    public int x;
    public int y;
    public int startX;
    public int startY;
    public int width;
    public int height;
    public boolean isDragged = false;
    public double translateX = 0;
    public double translateY = 0;

    public boolean isCurrentEntity() {
        return isCurrentEntity;
    }

    public void setCurrentEntity(boolean currentEntity) {
        isCurrentEntity = currentEntity;
    }

    private boolean isCurrentEntity = false;

    public double getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(double rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    private double rotationAngle = 0;
    private double scaleX = 1;

    public double getScaleY() {
        return scaleY;
    }

    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }

    public double getScaleX() {
        return scaleX;
    }

    public void setScaleX(double scaleX) {
        this.scaleX = scaleX;
    }

    private double scaleY = 1;
    public boolean isScaled() {
        return isScaled;
    }

    public void setScaled(boolean scaled) {
        isScaled = scaled;
    }

    private boolean isScaled = false;

    public boolean isRotated() {
        return isRotated;
    }

    public void setRotated(boolean rotated) {
        isRotated = rotated;
    }

    private boolean isRotated = false;

    public Shape getRotationShape() {
        return transformedRotationShape;
    }

    public Shape rotationShape;
    public Shape transformedRotationShape;

    public Shape getScalingShape() {
        return transformedScalingShape;
    }

    public void setScalingShape(Shape scalingShape) {
        this.scalingShape = scalingShape;
    }

    public Shape getTransformedScalingShape() {
        return transformedScalingShape;
    }

    public void setTransformedScalingShape(Shape transformedScalingShape) {
        this.transformedScalingShape = transformedScalingShape;
    }

    public Shape scalingShape;
    public Shape transformedScalingShape;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public JGeometry getjGeom() {
        return jGeom;
    }

    private JGeometry jGeom;

    public JGeometry getTransjGeom() {
        return transjGeom;
    }

    private JGeometry transjGeom;


    private boolean positionChanged = false;


    AffineTransform moveAt;
    AffineTransform recTrans;

    /**
     * Konstruktor pro MapEntity
     * @param shape tvar, ktery odpovídá jGeom
     * @param jGeom geometrie získaná z db
     * @param id id objektu v db
     * @param articleId id typu artiklu v db
     */
    public MapEntity(Shape shape, JGeometry jGeom, int id, int articleId)
    {

        this.transformedShape = shape;
        this.originalShape = shape;
        this.jGeom = jGeom;
        this.transjGeom = jGeom;
        this.id = id;
        rect = transformedShape.getBounds();
        x = rect.x;
        y = rect.y;
        width = rect.width;
        height = rect.height;
        //System.out.println(transformedShape.getClass());
        moveAt = new AffineTransform();
        this.transformedShape = moveAt.createTransformedShape(originalShape);
        color = Color.BLACK;
        rotationShape = new Ellipse2D.Double(originalShape.getBounds2D().getCenterX(), originalShape.getBounds2D().getY() - 15,10, 10);
        transformedRotationShape  = new Ellipse2D.Double(originalShape.getBounds2D().getCenterX(), originalShape.getBounds2D().getY() - 15,10, 10);
        scalingShape = new Rectangle((int)originalShape.getBounds2D().getCenterX(),
                (int)originalShape.getBounds2D().getY() + 15 + originalShape.getBounds().height, 10, 10);
        transformedScalingShape = new Rectangle((int)originalShape.getBounds().getCenterX(),
                (int)originalShape.getBounds2D().getY() + 15 + originalShape.getBounds().height, 10, 10);

        this.articleId = articleId;
    }

    /**
     * Kopírovací konstruktor pro MapEntity
     *
     * @param entity
     */
    public MapEntity(MapEntity entity)
    {
       this(entity.getOriginalShape(), entity.getjGeom(), entity.getId(), entity.getArticleId());
    }





    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * Metoda pro vykreslování objektu
     * Provádí transformace při rotacích, pohybu a  zvětšování-zmenšování
     *
     * @param g
     */
    public void paintEntity(Graphics g)
    {

        Graphics2D g2D = (Graphics2D) g;
        AffineTransform saveTransform = g2D.getTransform();
        moveAt = new AffineTransform(saveTransform);
        moveAt.translate(translateX, translateY);

        double rotationPointX = originalShape.getBounds2D().getX() + originalShape.getBounds2D().getWidth() / 2.0;
        double rotationPointY =  originalShape.getBounds2D().getY() + originalShape.getBounds2D().getHeight() / 2.0;
        moveAt.rotate(rotationAngle, rotationPointX, rotationPointY) ;
      //  System.out.println(rotationPointX);
       // System.out.println(rotationPointY);

       // moveAt.translate((400.0 + (originalShape.getBounds2D().getWidth() * scaleX) / 2.0),
        //        300 + (originalShape.getBounds2D().getHeight() * scaleY) / 2.0);

        double cx = originalShape.getBounds2D().getCenterX();
        double cy = originalShape.getBounds2D().getCenterY();

        moveAt.translate(cx,cy);
        moveAt.scale(scaleX, scaleY);
        moveAt.translate(-cx,-cy);
        g2D.setTransform(moveAt);
        g2D.setStroke(new BasicStroke(3));


        if (isSelected)
        {
            //rotationShape = new Rectangle(originalShape.getBounds().x - 10, originalShape.getBounds().y - 10, 10,10);
            color = Color.GREEN;
            g2D.setColor(color);
            g2D.fill(rotationShape);
            g2D.draw(rotationShape);

           // g.drawOval(originalShape.getBounds().x - 10, originalShape.getBounds().y - 10, 10,10);
            //g.fillOval(originalShape.getBounds().x - 10, originalShape.getBounds().y - 10, 10,10);

            g2D.fill(scalingShape);
            g2D.draw(scalingShape);


        }
        else {
           // color = Color.BLACK;
        }

        g2D.setPaint(Color.BLUE);
        g2D.fill(originalShape);
        g2D.setColor(color);
        g2D.draw(originalShape);


        g2D.setTransform(saveTransform);





        this.transformedShape = moveAt.createTransformedShape(originalShape);
        this.transformedRotationShape = moveAt.createTransformedShape(rotationShape);
        this.transformedScalingShape = moveAt.createTransformedShape(scalingShape);
    }

    public Rectangle getRectangle()
    {
        return this.rect;
    }

    public void setLoc()
    {

        transformedShape.getBounds().setLocation(new Point(200,200));


    }

    public Shape getTransformedShape()
    {
        return this.transformedShape;
    }

    public Shape getOriginalShape() {
        return originalShape;
    }

    public void setTransformedShape(Shape transformedShape) {
        this.transformedShape = transformedShape;
    }

    public void updateShape()
    {

        this.transformedShape = moveAt.createTransformedShape(transformedShape);
    }


    public boolean isPositionChanged() {
        return positionChanged;
    }

    public void setPositionChanged(boolean positionChanged) {
        this.positionChanged = positionChanged;
    }

    /**
     * Aktualizuje jGeometry objektu dle provedených transformací
     */
    public void updateJGeom()
    {

        double rotationPointX = originalShape.getBounds2D().getX() + originalShape.getBounds2D().getWidth() / 2.0;
        double rotationPointY =  originalShape.getBounds2D().getY() + originalShape.getBounds2D().getHeight() / 2.0;
        double[] rotationPointArr = {rotationPointX, rotationPointY};
        double[] scalePointArr = {0.0,0.0};
        try {
            transjGeom = jGeom.affineTransforms(true, translateX, translateY,  0,
                    true, JGeometry.createPoint(rotationPointArr,2,0), scaleX, scaleY, 0,
                    true , JGeometry.createPoint(rotationPointArr, 2,0) ,
                    null, rotationAngle ,-1,false,0,0,0,0,0,0,false,null,null,0,false,null,null);
        }

        catch (Exception e)
        {
            System.err.println("jGeom cannot be tranformed" + e.getMessage());
        }


    }
}
