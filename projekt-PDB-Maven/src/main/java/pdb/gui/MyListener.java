/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdb.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import oracle.ord.im.OrdImage;
import static pdb.gui.gui.addNahled;
import static pdb.gui.gui.detail_podobne;
import pdb.projekt.Databaze;
import pdb.projekt.localObjekt;

/**
 *
 * @author Radim Jílek, xjilek14
 */
class MyListener implements MouseListener, ActionListener {

	public MyListener() {
	}

	@Override
	public void mouseClicked(MouseEvent e) {


		String id = e.getComponent().getName();
		//System.out.println(""+id);
		showDetailDialog(Integer.parseInt(id));
	}

	@Override
	public void mousePressed(MouseEvent e) {
		return;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		return;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		return;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		return;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String name = e.getActionCommand();
		int index = name.indexOf(':');
		name = name.substring(index + 1);
		System.out.println(Integer.parseInt(name.trim()));

		showDetailDialog(Integer.parseInt(name.trim()));
	}

	private void showDetailDialog(int idArticle)
	{
		gui.setToReadWrite(false);
		if (gui.dialog_detail.isActive()) {
			gui.dialog_detail.dispose();
		}
		//String id = e.getComponent().getName();
		//System.out.println(""+id);
		try {
			localObjekt obj = (localObjekt) gui.items.get(idArticle);
			gui.setDetailInfo(Databaze.getFoto(obj.getId(), "foto"), obj,obj.getId());
		} catch (SQLException | IOException ex) {
			Logger.getLogger(MyListener.class.getName()).log(Level.SEVERE, null, ex);
		}
		detail_podobne.removeAll();
		try {
			Vector v = Databaze.najdiPodobne(idArticle);
			//System.out.println(v.toString());
			for (Iterator it = v.iterator(); it.hasNext();) {
				Integer pod_id = (Integer) it.next();
				OrdImage img = Databaze.getFoto(pod_id, "nahled");
				ImageIcon ico = null;
				if(img.getContentLength() != 0)
					ico = new ImageIcon(img.getDataInByteArray());
				detail_podobne.add(addNahled(ico, pod_id,((localObjekt)gui.items.get(pod_id)).getNazev()));
				gui.updateHorizontalSize();
			}
		} catch (SQLException | IOException ex) {
			Logger.getLogger(gui.class.getName()).log(Level.SEVERE, null, ex);
		}
		gui.dialog_detail.revalidate();
		gui.dialog_detail.show();

	}
}
