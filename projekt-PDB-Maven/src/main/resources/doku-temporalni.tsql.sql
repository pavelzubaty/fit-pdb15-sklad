#a)v�b�r ur�it�ho typu objektu platn�ho od-do
 VALIDTIME PERIOD[2015/10/10-2015/11/25] SELECT * FROM article WHERE article_type = 2;
 #SQL (v tride Temporal pro vice typu)
 SELECT * FROM article WHERE typ='K�eslo' AND 
  ((dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD')) OR
   (dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD')));
#  vyber artiklu platnych od-do (trivialni)
 VALIDTIME PERIOD[2015/10/10-2015/11/11] SELECT * FROM article;
 #SQL
SELECT * FROM article WHERE
  ((dostupne_od>=TO_DATE('10/10/2015','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD')) OR
   (dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD')));
#b)vyber vsech artikluu daneho dodavatele, ktery ma smlouvu v obdobi od-do
  VALIDTIME PERIOD[2015/10/10-2015/11/25] SELECT a.* FROM article a, suplier s
  WHERE s.id=1 AND a.dodavatel=s.id;
  #SQL
SELECT a.* FROM article a, suplier s
  WHERE s.id=1 AND a.dodavatel=s.id AND
  ((s.smlouva_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND s.smlouva_do<=TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (s.smlouva_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND s.smlouva_do>TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (s.smlouva_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND s.smlouva_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND s.smlouva_do>=TO_DATE('2015-10-10','YYYY-MM-DD')) OR
   (s.smlouva_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND s.smlouva_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND s.smlouva_od<=TO_DATE('2016-11-25','YYYY-MM-DD')));
#  vyber vsech dodavateluu daneho typu artiklu dostupnych v obdobi od-do
  VALIDTIME PERIOD[2015/10/10-2015/11/25] SELECT s.* FROM suplier s, article a
  WHERE a.typ='�idle' AND a.dodavatel=s.id; 
  #SQL
  SELECT s.* FROM suplier s, article a
  WHERE a.typ='�idle' AND a.dodavatel=s.id AND
  ((a.dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD')) OR
   (a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD')) OR
   (a.dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD')));  


#c)zmena dodavatele pro artikl v obdobi od-do
  VALIDTIME PERIOD[2015/10/10-2015/11/25] UPDATE article SET dodavatel=4 WHERE id=12;
  #SQL
  #interval cely v zadanem obdobi
  UPDATE article SET dodavatel=4
  WHERE id=12 AND dodavatel<>4 AND
  dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD');
  
  #interval je delsi zleva i zprava nez zvolene obdobi
  INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)
  SELECT a.nazev, a.geometrie, a.typ, a.dostupne_od, TO_DATE('2015-10-10','YYYY-MM-DD') - 1/86400,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel
  FROM article a WHERE a.id=12 AND a.dodavatel<>4 AND a.dostupne_od < TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do > TO_DATE('2016-11-25','YYYY-MM-DD');
  
  INSERT INTO article (nazev, geometrie, typ, dostupne_od, dostupne_do,popis, nahled, foto,foto_si, foto_ac,foto_ch, foto_pc, foto_tx, dodavatel)
  SELECT a.nazev, a.geometrie, a.typ, TO_DATE('2016-11-25','YYYY-MM-DD') + 1/86400, a.dostupne_do,a.popis, a.nahled, a.foto,a.foto_si, a.foto_ac,a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel
  FROM article a WHERE a.id=12 AND a.dodavatel<>4 AND a.dostupne_od < TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do > TO_DATE('2016-11-25','YYYY-MM-DD');
  
  UPDATE article SET dodavatel=4, dostupne_od=TO_DATE('2015-10-10','YYYY-MM-DD'), dostupne_do=TO_DATE('2016-11-25','YYYY-MM-DD')
  WHERE id=12 AND dodavatel<>4 AND
  dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD');
  
  #interval zleva delsi nez zadane obdobi
  INSERT INTO article
  (nazev, geometrie, typ, dostupne_od, dostupne_do,	popis, nahled, 	foto,	foto_si, foto_ac,	foto_ch, foto_pc, foto_tx,dodavatel)
  SELECT a.nazev, a.geometrie, a.typ, a.dostupne_od, TO_DATE('2015-10-10','YYYY-MM-DD') - 1/86400,	a.popis, a.nahled, a.foto,	a.foto_si, a.foto_ac,	a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel
  FROM article a WHERE a.id=12 AND a.dodavatel<>4 AND a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD');
  
  UPDATE article SET dodavatel=4, dostupne_od=TO_DATE('2015-10-10','YYYY-MM-DD'), dostupne_do=TO_DATE('2016-11-25','YYYY-MM-DD')
  WHERE id=12 AND dodavatel<>4 AND
  a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD');
  
  #interval zprava delsi nez zadane obdobi
  INSERT INTO article
  (nazev, geometrie, typ, dostupne_od, dostupne_do,	popis, nahled, 	foto,	foto_si, foto_ac,	foto_ch, foto_pc, foto_tx,dodavatel)
  SELECT a.nazev, a.geometrie, a.typ, TO_DATE('2016-11-25','YYYY-MM-DD') + 1/86400, a.dostupne_do,	a.popis, a.nahled, a.foto,	a.foto_si, a.foto_ac,	a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel
  FROM article a WHERE a.id=12 AND a.dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD');
  
  UPDATE article SET dodavatel=4, dostupne_od=TO_DATE('2015-10-10','YYYY-MM-DD'), dostupne_do=TO_DATE('2016-11-25','YYYY-MM-DD')
  WHERE id=12 AND dodavatel<>4 AND  
  a.dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD');
  
  
#d)vymazani artiklu v obdobi od-do
  VALIDTIME PERIOD[2015/10/10-2015/11/25] DELETE FROM article WHERE id=12;
#SQL
  #artikl cely v intervalu
  DELETE FROM article WHERE id=12 
  AND dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD');
  
  #artikl presahuje mazany interval z obou stran
  INSERT INTO article
  (nazev, geometrie, typ, dostupne_od, dostupne_do,	popis, nahled, 	foto,	foto_si, foto_ac,	foto_ch, foto_pc, foto_tx, dodavatel)
  SELECT a.nazev, a.geometrie, a.typ, TO_DATE('2016-11-25','YYYY-MM-DD') + 1/86400, a.dostupne_do,	a.popis, a.nahled, a.foto,	a.foto_si, a.foto_ac,	a.foto_ch, a.foto_pc, a.foto_tx, a.dodavatel
  FROM article a WHERE a.id=12 AND a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD');
  
  UPDATE article SET dostupne_do=TO_DATE('2015-10-10','YYYY-MM-DD') - 1/86400
  WHERE id=12 AND
  dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD');
  
  #artikl presahuje zleva mimo interval
  UPDATE article SET dostupne_do=TO_DATE('2015-10-10','YYYY-MM-DD') - 1/86400
  WHERE id=12 AND
  a.dostupne_od<TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do<=TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_do>=TO_DATE('2015-10-10','YYYY-MM-DD');
  
  #artikl presahuje zprava mimo interval
  UPDATE article SET dostupne_od=TO_DATE('2016-11-25','YYYY-MM-DD') + 1/86400
  WHERE id=12 AND
  a.dostupne_od>=TO_DATE('2015-10-10','YYYY-MM-DD') AND a.dostupne_do>TO_DATE('2016-11-25','YYYY-MM-DD') AND a.dostupne_od<=TO_DATE('2016-11-25','YYYY-MM-DD');